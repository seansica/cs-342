package prime;

import queue.ArrayQueue;
import java.util.Iterator;
import java.util.Scanner;


public class PrimeSieve {

    public static void main(String[] args){

        // Query the user for the max number
        int n = getMaxIntN();

        // Instantiate an array-queue
        ArrayQueue allNums = new ArrayQueue();


        // Fill it with the consecutive integers 2 through n inclusive.
        for (int i = 2; i <= n; i++) {

            allNums.enqueue(i);
        }

        // Instantiate an empty queue to store the prime numbers
        ArrayQueue primes = new ArrayQueue();


        // Calculate and store prime numbers
        sieveOfEratosthenes(allNums, primes);


        System.out.println("*** QUEUE OF PRIMES ***");
        //primes.display();
        Iterator iter = primes.iterator();
        for(int i = 0; i < primes.size(); i++){

            if(i % 10 == 0){

                System.out.println();
            }

            System.out.printf("%-5d", iter.next());
        }

    }


    /**
     * Calculates all prime numbers from 2 to N
     * @param allNums list of all integers '{2..N}' to be parsed
     * @param primes computed list of prime numbers
     */
    private static void sieveOfEratosthenes(ArrayQueue allNums, ArrayQueue primes) {

        Iterator iter = allNums.iterator();

        Integer p = (Integer) iter.next(); // 2

        primes.enqueue(p);

        //System.out.println("first prime p=" + p);

        int size = allNums.size() + 1;

        //System.out.println("size=" + size);

        int n; // placeholder for prime candidate

        do{

            // check all numbers from 2 to N
            for (int i = 2; i < size; i++) {

                // Flag to determine if N is divisible by already-flagged prime number
                boolean isPrime = true;

                // Set prime candidate if queue of N has number remaining
                if(iter.hasNext()){
                    n = (int) iter.next() - 1;
                }
                else{
                    return;
                }

                // Check if i is divisible by all numbers from 2 to the sqrt(N)
                if((i % p != 0 && i != p)){

                    // check if i is divisible by any already-flagged prime number
                    for(Object pr: primes){

                        // Only check numbers larger than already-flagged primes
                        if(i > (int)pr){

                            // Set prime to false if i is divisible by any number in list of primes
                            if(i % (int)pr == 0){
                                isPrime = false;
                            }
                        }
                    }
                    // add prime candidate to list of primes if not divisible by any number in queue of 2..N
                    if(isPrime)
                        primes.enqueue( n );
                }
            }
            // Increase p from 2..sqrt(N)
            p++;

        }while(p < sqrt(size));
    }


    /**
     * Calculates and returns the square root of a given number
     * @param num Number to be square-rooted
     * @return the square root result
     */
    private static double sqrt(int num) {

        double t;

        double squareRoot = num / 2;

        do {
            t = squareRoot;
            squareRoot = (t + (num / t)) / 2;
        } while ((t - squareRoot) != 0);

        return squareRoot;
    }


    /**
     * Prompts the user for a number, N.
     * @return the maximum number from 2 to N from which to calculate prime numbers
     */
    private static int getMaxIntN(){

        Scanner input = new Scanner(System.in);
        System.out.print("Calculate all prime numbers up to: ");
        String n = input.nextLine();

        if(!isNumeric(n)) {
            System.out.println("Only numeric values are accepted.");
            return -1;
        }

        return Integer.parseInt(n);
    }



    /**
     * Determines whether a given String contains only numeric digits or not
     * @param str is the given string to the assessed
     * @return True is the given string only contains numerical digits; otherwise return false
     */
    private static boolean isNumeric(String str){

        // null or empty
        if (str == null || str.length() == 0) {
            return false;
        }

        for (char c : str.toCharArray()) {
            if (!Character.isDigit(c)) {
                return false;
            }
        }
        return true;
    }

}
