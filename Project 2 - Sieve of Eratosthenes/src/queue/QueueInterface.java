package queue;

public interface QueueInterface<T> {

    // FIFO

    /**
     * Insert an item into the queue
     * @param data Element to be inserted
     * @return True if insertion successful
     */
    public boolean enqueue(T data);

    /**
     * Removes the first item in the queue
     * @return First item in the queue
     */
    public T dequeue();

    /**
     * Gets the first item in the queue without removing it
     * @return First item in the queue
     */
    public T peek();

    /**
     * Determines if queue contains elements or not
     * @return True if no elements in queue
     */
    public boolean isEmpty();

    /**
     * Determines if queue has available capacity remaining for new elements
     * @return False if count equals capacity
     */
    public boolean isFull();

    /**
     * Prints the contents of the queue to the console
     */
    public void display();

    /**
     * Returns the size of the queue as determined by the current number of elements contained
     * @return
     */
    public int size();

}
