package queue;

import java.util.Iterator;


@SuppressWarnings("unchecked")

public class ArrayQueue<T> implements QueueInterface<T>, Iterable<T> {


    private static final int DEFAULT_CAPACITY = 10;

    private int  maxCapacity; // total number of elements in the queue
    private int     curCount; // current number of elements
    private int         head; // front index
    private int         tail; // back index

    private T[] Q; // array-based queue


    /**
     * Default constructor
     * Creates a new empty queue.
     */
    public ArrayQueue() {
        maxCapacity = DEFAULT_CAPACITY;
        Q = (T[]) new Object[DEFAULT_CAPACITY];
        tail = -1;
        head = 0;
    }



    /**
     * Tests if the queue is empty
     * @return True if the queue is empty and False otherwise
     */
    public boolean isEmpty() {
        return curCount == 0;
    }



    /**
     * Puts a value into the back of the queue.
     * Wrap-around supported
     * If the queue is full, it doubles its size.
     * @param data the item to insert.
     * @return True if enqueue operation is successful; False otherwise
     */
    public boolean enqueue(T data) {

        if (isFull()) {
            doubleSize();
        }

        tail++;
        Q[tail % maxCapacity] = data;
        curCount++;
        return true;
    }


    /**
     * Returns the first element in the queue.
     * @return element at front of the queue
     * @throws NoSuchElementException if the queue is empty.
     */
    public T peek() {
        if (isEmpty())
            throw new QueueException();
        else
            return Q[head % maxCapacity];
    }


    /**
     * Returns and removes the front element of the queue.
     * @return element at front of the queue
     */
    public T dequeue() {
        T e = peek();
        Q[head % maxCapacity] = null; // for garbage collection
        head++;
        curCount--;
        return e;
    }


    /**
     * Returns the size of the queue as determined by the current number of elements contained
     * @return
     */
    @Override
    public int size() {
        return curCount;
    }


    /**
     * Prints the contents of the queue to the console
     */
    @Override
    public void display() {

        // Display 10 elements per line

        String rtn = "";

        if (curCount == 0) {
            rtn += "<Empty>";
        } else {
            int n = head;
            int c = curCount;

            while (c > 0) {
                if (n == head) {
                    rtn += "head -> ";
                } else {
                    rtn += "        ";
                }

                rtn += Q[n] + "\n";
                n++;
                if (n == Q.length) {
                    n = 0;
                }

                c--;
            }

            System.out.println(rtn);
        }
    }



    /**
     * Determines if queue has available capacity remaining for new elements
     * @return False if count equals capacity
     */
    public boolean isFull() {
        return curCount == maxCapacity;
    }



    /**
     * Increase the queue capacity by doubling the size.
     */
    private void doubleSize() {
        T[] newArray = (T[]) new Object[2 * maxCapacity];

        //copy items
        for (int i = head; i <= tail; i++)
            newArray[i - head] = Q[i % maxCapacity];

        Q = newArray;
        head = 0;
        tail = curCount - 1;
        maxCapacity *= 2;
    }


    public void overWrite(ArrayQueue copyFrom) {

        for (int i = 0; i < maxCapacity; i++) {

            if (!(copyFrom.peek() == null))
                Q[i] = (T) copyFrom.dequeue();
        }
    }



    /**
     * Creates an iterator structure to traverse the queue from head to tail
     * @return an iterator object
     */
    public Iterator<T> iterator() {
        return new QIterator();
    }



    /***********************************************
     *************** NESTED ITERATOR ***************
     ***********************************************/

    private class QIterator implements Iterator<T> {


        private int index;      //traversal index


        /**
         * Create a new empty iterator.
         */
        public QIterator() {
            index = head;
        }

        /**
         * Tests if there are more items in the Queue
         */
        public boolean hasNext() {
            return index <= tail;
        }

        /**
         * Returns the next item in the Queue.
         */
        public T next() {

            //T rtn = A[(index)%cap]; // capture cur item
            //A[(index)%cap] = null; // set cur to null
            //index++; // inc index
            //return rtn;

            return Q[(index++) % maxCapacity];
        }

        /**
         * Do nothing if queue is empty, otherwise increase index by one
         */
        public void remove() {
            if (isEmpty())
                return;

            if (hasNext())
                index++;

            //T rtn = A[index];
            //A[index] = null;
            //return rtn;
        }
    }
}