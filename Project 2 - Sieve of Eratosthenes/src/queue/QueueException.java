package queue;

public class QueueException extends RuntimeException {

    public QueueException(String name) {
        super(name);
    }

    public QueueException() {
        super();
    }
}