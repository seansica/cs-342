# Linked List Implementation

This is a class implementation of both a singly linked list, and a doubly linked list.

*SNode* (singly-linked node) and *DNode* (doubly-linked Node) are sub-classes of Node. The only difference is that DNode maintains a reference to two Nodes, `next` and `previous`, as opposed to SNode which just maintains a reference to `next`.

### Features

- Insert new nodes anywhere in the list
- Delete existing nodes anywhere in the list
- Search and find any node in the list
- Print node contents of entire list

