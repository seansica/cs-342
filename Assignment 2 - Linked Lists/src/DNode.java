public class DNode extends Node {

    private DNode prev;
    private DNode next;

    public DNode(){
        super();
        prev = null;
        next = null;
    }

    public DNode(String data){
        super();
        this.setData(data);
    }

    public DNode getPrev(){
        return prev;
    }

    public DNode getNext(){
        return next;
    }

    public void setPrev(DNode prev){
        this.prev = prev;
    }

    public void setNext(DNode next){
        this.next = next;
    }


}
