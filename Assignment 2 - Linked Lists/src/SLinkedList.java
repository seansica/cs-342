
public class SLinkedList {

    private SNode head;

    /**
     * This method creates a new SNode containing a String and appends it to the end of the Linked List
     * @param data This is a String object to be encapsulated within a new SNode
     */
    public void insert(String data){

        SNode newSNode = new SNode(data);    // 1. Instantiate new SNode to contain data argument
        /*
         * Option 1:
         * If list is empty, then newSNode becomes the Head
         */
        if(head == null){
            head = newSNode;
        }
        /*
         * Option 2:
         * 1. Make a copy of head
         * 2. Hop from node to next node continuously, until last node is reached
         * 3. Once last node reached, append new SNode to end of the list
         */
        else{
            SNode n = head;               // 1
            while(n.getNext() != null){   // 2
                n = n.getNext();          // 2
            }
            n.setNext(newSNode);          // 3
            newSNode.setNext(null);
        }
    }



    /**
     * This method instantiates a new SNode and installs it at the front of the list
     * The Head will reference the new SNode
     * @param data This is a String object to be encapsulated within a new SNode
     */
    public void insertAtStart(String data){

        SNode newHead = new SNode(data); // 1. Instantiate new SNode to contain new data
        newHead.setNext(head);         // 2. Point new SNode to current head
        head = newHead;                // 3. Point head to new SNode
    }



    /**
     * This method instantiates a new SNode and installs it at a specified location in the list
     * @param refData This is the reference String used to determine where to install the new SNode.
     *                The new SNode is installed so that it precedes the reference SNode by one hop/degree
     * @param data This is a String object to be encapsulated within a new SNode
     */
    public boolean insertBefore(String refData, String data){

        if(!find(refData))
            return false;

        SNode newSNode = new SNode(data);              // 1. Instantiate new SNode to contain new data
        SNode n = head;                              // 2. Clone head and call it n

        if(head.getData().equals(refData)){
            insertAtStart(data);
            return true;
        }

        while(!n.getNext().getData().equals(refData)){
            n = n.getNext();                        // 3. Use n to traverse list.
        }                                           // 4. Stop traversing when the next node is the reference SNode
        newSNode.setNext(n.getNext());               // 5. Point new SNode to next SNode's next
        n.setNext(newSNode);                         // 6. Point reference SNode to new SNode
        return true;
    }



    /**
     * This method determines whether a specific data String is contained within the list
     * @param data This is the reference data with which we will search the list
     * @return Return true if specified data is contained within the list,
     *         or return false is specified data is not contained within the list
     */
    public boolean find(String data){

        SNode n = head;

        while(n != null){

            // Check current node
            if(n.getData().equals(data)){
                return true;
            }
            n = n.getNext();  // Move to next node
        }
        return false;
    }



    /**
     * This method searches the linked list for a specified node
     * @param data This is the reference data that the search result is based on
     * @return This is the node match containing the specified data
     */
    public SNode search(String data){

        SNode n = head;

        while(n != null){

            // Check current node
            if(n.getData().equals(data)){
                return n;
            }
            n = n.getNext();
        }
        return null;
    }



    /**
     * This method deletes a specified node from the Linked List
     * @param data This is the reference data used to match the candidate deletion node
     * @return Returns true if node deletion successful. Returns False if reference data not located in list
     */
    public boolean delete(String data){

        // Stop if data not found
        if(!find(data)) {
            return false;
        }
        // Data found!
        SNode deleteMe = search(data);

        SNode n = head;

        // deleteMe -> head.next -> ...
        if(deleteMe == head) {
            head = head.getNext();
            return true;
        }

        // n -> n.next -> ... -> deleteMe
        while(n.getNext() != deleteMe){
            n = n.getNext();
        }

        // n -> deleteMe -> deleteMe.next
        n.setNext(deleteMe.getNext());
        return true;
    }



    /**
     * Prints data contents of all SNodes contained in the Linked List
     */
    public void show(){

        String list = "";
        SNode node = head;  // Make a copy of head to traverse the list

        while(node != null){

            // Print the contents of the current iteration of SNode

            if(node == head) {
                // Append head tag to head node
                list += "head -> ";
            }

            else {
                list += "        ";
            }

            // Encapsulate elements in angular brackets < >
            list += "<" + node.getData() + ">\n";

            node = node.getNext();
        }
        System.out.println(list);
    }


}
