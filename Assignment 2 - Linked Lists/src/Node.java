public abstract class Node {

    private String data;

    public Node(){
        this.data = "";
    }

    public Node(String data){
        this.data = data;
    }

    public String getData(){
        return this.data;
    }

    public void setData(String data){
        this.data = data;
    }

}
