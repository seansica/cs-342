public class DLinkedList {

    private DNode head;
    private DNode tail;


    /**
     * This method creates a new DNode containing a String and appends it to the end of the Linked List
     * @param data This is a String object to be encapsulated within a new DNode
     */
    public void insert(String data){

        DNode newDNode = new DNode(data); // Instantiate new DNode to contain data argument

        /*
         * Option 1:
         * If list is empty, then newDNode becomes head and tail
         */
        if(head == null){
            head = newDNode;
            tail = newDNode;
        }
        /*
         * Option 2:
         * If list is not empty,
         * 1. Iterate until last node
         * 2. Make newNode the tail
         * 3. Point newNode.next to null
         * 4. Point newNode.prev to oldTail
         * 5. Point oldTail.next to new tail
         */
        else{
            DNode cur = head;
            DNode prev = null;
            while(cur.getNext() != null){
                prev = cur;
                cur = cur.getNext();
            }
            cur.setPrev(prev);
            tail = newDNode;
            cur.setNext(tail);
            tail.setNext(null);
            tail.setPrev(cur);
        }

    }



    /**
     * This method instantiates a new DNode and installs it at the front of the list
     * The Head will reference the new DNode
     * @param data This is a String object to be encapsulated within a new DNode
     */
    public void insertAtStart(String data){

        DNode newHead = new DNode(data);
        newHead.setPrev(null);
        newHead.setNext(head);
        head = newHead;

    }



    /**
     * This method instantiates a new DNode and installs it at a specified location in the list
     * @param refData This is the reference String used to determine where to install the new DNode.
     *                The new DNode is installed so that it precedes the reference DNode by one hop/degree
     * @param data This is a String object to be encapsulated within a new DNode
     */
    public boolean insertBefore(String refData, String data){

        if(!find(refData))
            return false;

        DNode newDNode = new DNode(data);
        DNode cur = head;

        if(head.getData().equals(refData)){
            insertAtStart(data);
            return true;
        }

        while(!cur.getNext().getData().equals(refData))
            cur = cur.getNext();

        cur.getNext().setPrev(newDNode);
        newDNode.setNext(cur.getNext());
        cur.setNext(newDNode);
        newDNode.setPrev(cur);

        return true;

    }



    /**
     * This method determines whether a specific data String is contained within the list
     * @param data This is the reference data with which we will search the list
     * @return Return true if specified data is contained within the list,
     *         or return false is specified data is not contained within the list
     */
    public boolean find(String data){

        DNode n = head;

        while(n != null){

            if(n.getData().equals(data))
                return true;

            n = n.getNext();
        }
        return false;

    }



    /**
     * This method searches the linked list for a specified node
     * @param data This is the reference data that the search result is based on
     * @return This is the node match containing the specified data
     */
    public DNode search(String data){

        DNode n = head;

        while(n != null){

            // Check current node
            if(n.getData().equals(data))
                return n;

            n = n.getNext();
        }
        return null;

    }


    /**
     * This method deletes a specified node from the Linked List
     * @param data This is the reference data used to match the candidate deletion node
     * @return Returns true if node deletion successful. Returns False if reference data not located in list
     */
    public boolean delete(String data){

        // Stop if data not found
        if(!find(data))
            return false;

        // Data found!
        DNode deleteMe = search(data);

        DNode n = head;

        if(deleteMe == head){
            head = head.getNext();
            head.setPrev(null);
            return true;
        }

        while(n.getNext() != deleteMe)
            n = n.getNext();

        n.setNext(deleteMe.getNext());

        if(deleteMe.getNext() == null) {
            //n.setNext(deleteMe.getNext());
            tail = n;
        }
        else{
            //n.setNext(deleteMe.getNext());
            deleteMe.getNext().setPrev(n);
        }

        return true;

    }



    /**
     * Prints data contents of all DNodes contained in the Linked List
     */
    public void show(){
        String list = "";
        DNode node = head;

        while(node != null){

            if(node == head) {
                list += "head -> ";
            }
            else if(node == tail) {
                list += "tail -> ";
            }
            else {
                list += "        ";
            }
            list += "<" + node.getData() + ">\n";

            node = node.getNext();
        }
        System.out.println(list);

    }



}
