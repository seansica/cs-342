public class Main {

    public static void main(String[] args){

        /*
         * GENERATE REQUIRED PROGRAM INPUT (10 STRINGS)
         */
        String[] input = {"one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten"};

        /*
         * INSTANTIATE LINKED LIST
         * Comment out either SLinkedList or DLinkedList to demonstrate
         * the desired list type
         */
        SLinkedList list = new SLinkedList();
        //DLinkedList list = new DLinkedList();

        /*
         * POPULATE THE SINGLY LINKED LIST
         * I guess you can call this cheating because I'm
         * iterating over an Array to consolidate lines
         * of code.
         */
        for(String i: input){
            list.insert(i);
        }

        // Demonstrate ability to print entire list
        System.out.println("*** SHOW ***");
        list.show();
        System.out.println();


        // Demonstrate ability to add node to beginning of list
        System.out.println("*** INSERT AT START ***");
        list.insertAtStart("zero");
        list.show();
        System.out.println();


        // Demonstrate ability to add node before a specified node in the list
        System.out.println("*** INSERT BEFORE ***");
        String insertMe = "derp";
        String beforeMe = "zero";
        if(list.insertBefore(beforeMe, insertMe))
            System.out.println("Inserted " + insertMe + " before " + beforeMe);
        list.show();
        System.out.println();



        // Demonstrate ability to search for a specific node in the list
        System.out.println("*** SEARCH FOR SPECIFIC NODE ***");
        String findMe = "ten";
        System.out.println("Searching for " + findMe + "...");

        if(list.find(findMe)) {
            Node found = list.search(findMe);
            System.out.println("Search result: " + found.getData() + " found!");
        }
        else{
            System.out.println("Search result: NOT FOUND");
        }
        System.out.println();


        // Demonstrate ability to delete a specified node
        System.out.println("*** DELETE SPECIFIED NODE ***");
        String deleteMe = "ten";
        if(list.find(deleteMe)){
            if(list.delete(deleteMe)){
                System.out.println(deleteMe + " deleted!");
                list.show();
            }
            else{
                System.out.println("Error: Node not deleted!");
            }
        }
        else
            System.out.println("Error: Node not found!");

        System.out.println();
    }


}
