public class SNode extends Node {

    private SNode next;

    public SNode(){
        super();
        next = null;
    }

    public SNode(String data){
        this.setData(data);
        next = null;
    }

    public SNode getNext(){
        return next;
    }

    public void setNext(SNode next){
        this.next = next;
    }
}
