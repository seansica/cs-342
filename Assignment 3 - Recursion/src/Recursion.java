public class Recursion {


    private static final String SYMBOL = "*";

    private static int SIZE = 4;



    public static void main(String[] args){

        /*
         * Write a recursive method to produce a pattern of n lines of
         * asterisks. The first line contains one asterisk, the next line
         * contains 2, and so on, up to the nth line, which contains n
         * asterisks Line n+1 and n+2 contain n asterisks. The next
         * line has n-1 asterisks, and so on until line number 2n+1
         * which has 1 asterisk. You can use a recursive method, an
         * additional parameter, or a loop to print the asterisks. (50 Points)
         */
        buildPattern(SIZE, 1, SYMBOL);

        /*
         * Write a recursive method to return an integer as a String of
         * digits. You can pass as many arguments as you please, an
         * example could be:
         * convert(1234);
         * This call returns “1234”.
         * (50 points)
         */
        System.out.println(new String(intToString(987654321)));


    }



    /**
     * This method recursively builds a pyramid structure using a pattern of n lines of
     * symbols. The first line contains one symbol. The next line contains 2, and so on,
     * up to the nth line, which contains n symbols. Line n+1 and n+2 contain n symbols.
     * The next line has n-1 symbols, and so on until line number 2n+1 which has 1 symbols.
     * @param n Refers to the maximum amount of symbols that can be contained in a row
     * @param i Refers to a counter that is used to track the recursion
     * @param str Refers to character symbol to be used in the pattern
     */
    private static void buildPattern(int n, int i, String str) {

        /*
         *
         * n=3, size = 5  (diff 2) or n+2 or n+(n-1) or 2n-1
         * n=4, size = 7  (diff 3) or n+3 or 2n-1
         * n=5, size = 9  (diff 4) or n+4 or 2n-1
         * n=6, size = 11 (diff 5) or n+5 or 2n-1
         *
         *  I
         *  9 *     n=1 = n-3
         *  8 **    n=2 = n-2
         *  7 ***   n=3 = n-1
         *  6 ****  n=4 = n
         *  5 ****  n=4 = n
         *  4 ****  n=4 = n
         *  3 ***   n=3 = n-1
         *  2 **    n-2 = n-2
         *  1 *     n-3 = n-3
         */

        if(n < 1)
            return;

        else if(i < n){
            printEachRow(i, str);
            buildPattern(n, ++i, str);
        }

        else if(i == n || i == n+1 || i == n+2){
            printEachRow(n, str);
            buildPattern(n, ++i, str);
        }

        else if(i > n){
            printEachRow(--n, str);
            buildPattern(n, ++i, str);

        }
    }

    /**
     * This method recursively builds a string of n asterisks
     * @param n Refers to the number of asterisks to concatenate
     * @param str Refers to the string to which asterisks are being joined
     */
    private static void printEachRow(int n, String str){

        if(n < 1){
            System.out.println("");
            return;
        }
        System.out.print(str);
        printEachRow(n-1, str);
    }

    /**
     * This method recursively converts an Integer to a String using recursion.
     * It breaks down an integer by first dividing it by 10 (e.g. 125/10=12.5). Then
     * it performs modulus-10 math on the quotient (e.g. 12.5 % 10 = 2.5). That result is
     * recursively passed back into itself until the result equals zero, at which point the recursion
     * is complete and the stack pops the results off the stack.
     * @param n Refers to the Integer that should be converted
     * @return Refers to the String value that has been converted.
     */
    public static String intToString( int n ) {

        if ( n == 0 ) {
            return "";
        } else {
            return intToString( n / 10 ) + ( n % 10 );
        }
    }


}
