import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class NumberGuesser {

    // CONSTANTS
    private static final int MIN_RANGE = 1;
    private static final int MAX_RANGE = 1000;
    private static final int RANDOM_NUMBER = ThreadLocalRandom.current().nextInt(MIN_RANGE,MAX_RANGE + 1);


    // MAIN METHOD
    public static void main(String[] args){

        System.out.println("I am thinking of a number between " + MIN_RANGE + " and " + MAX_RANGE + ". What is it?");

        // Initialize scanner to collect user input
        Scanner userInput = new Scanner(System.in);

        // ATTRIBUTES
        boolean matchNotFound = true;
        int guessTracker = 0;

        // User do/try block instead of while block because user must guess at least once
        do{

            // Display current number of guesses
            System.out.println("Current number of guesses: " + guessTracker);

            // Prompt for user input
            System.out.print("Enter guess: ");
            String unvalidatedUserGuess = userInput.nextLine();

            // Increment total number of guesses
            guessTracker++;

            // The purpose of the try block to handle various invalid argument exceptions thrown by other methods
            try{

                // Validate whether user input only numbers
                Integer validatedUserGuess = validateUserInput(unvalidatedUserGuess);

                // Handle correct guess
                if(matches(validatedUserGuess, RANDOM_NUMBER)){
                    System.out.println("MATCH FOUND! It took you " + guessTracker + " guesses!");
                    matchNotFound = false;
                }
            }
            catch(IllegalArgumentException e){
                System.out.println(e.getMessage());
            }

        }
        while(matchNotFound); // Continue looping until matchNotFound is false, or in other words, until match is found


    }


    /**
     * This method validates whether a supplied String argument contains only digits/numerical values
     * @param userInput The argument is a String supplied by the userInput Scanner from the Main method
     * @return Returns an Integer containing the corresponding digits in the argument IFF argument only contains digits; otherwise throws IllegalArgumentException
     */
    private static Integer validateUserInput(String userInput){

        // PARAMS
        Integer number;

        // Handle
        if(!userInput.matches("[0-9]+"))
            throw new IllegalArgumentException("Error! Argument must only contain numbers.");

        // POST CONDITION
        number = Integer.parseInt(userInput);
        return number;

    }

    /**
     * This method checks whether two integers contain the same numerical values
     * @param guess The number that method is checking
     * @param RANDOM_NUMBER The number to which method is referencing/validating against
     * @return boolean is true if argument 1 matches argument 2; otherwise boolean is false
     */
    private static boolean matches(int guess, int RANDOM_NUMBER){

        if(guess < MIN_RANGE)
            throw new IllegalArgumentException("Error! Guess is too low.");

        if(guess > MAX_RANGE)
            throw new IllegalArgumentException("Error! Guess is too high.");

        if(guess < RANDOM_NUMBER){
            System.out.println("Guess again. Higher!");
            return false;
        }

        if(guess > RANDOM_NUMBER){
            System.out.println("Guess again. Lower!");
            return false;
        }
        return true;
    }
}

