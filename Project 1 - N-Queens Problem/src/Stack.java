public interface Stack {

    boolean push(Integer row, Integer column, Integer value);

    Integer pop();

    boolean isEmpty();

    default boolean isFull(){
        return false;
    }

    Integer peek();

    int size();

}
