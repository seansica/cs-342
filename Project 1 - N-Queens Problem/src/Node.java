public class Node<E> {

    private E row;
    private E column;
    private E value;
    private Node<E> next;
    public static int test = 0;

    /**
     * Default constructor
     * @param column
     */
    public Node(E row, E column, E value){
        this.row = row;
        this.column = column;
        this.value = value;
        this.next = null;
    }

    // GETTER
    public E getRow(){
        return this.row;
    }

    // GETTER
    public E getColumn(){
        return this.column;
    }

    // GETTER
    public E getValue(){
        return this.value;
    }

    // GETTER
    public Node<E> getNext(){
        return this.next;
    }

    // SETTER
    public void setRow(E row){
        this.row = row;
    }

    // SETTER
    public void setColumn(E column){
        this.column = column;
    }

    // SETTER
    public void setValue(E value){
        this.value = value;
    }

    // SETTER
    public void setNext(Node<E> next){
        this.next = next;
    }

}
