public class SLinkedStack implements Stack {

    private Node<Integer> top;
    private int size;


    /**
     * Default constructor
     */
    public SLinkedStack(){
        top = null;
        size = 0;
    }

    @Override
    public boolean push(Integer row, Integer column, Integer value) {
        Node<Integer> newNode = new Node<>(row, column, value);
        newNode.setNext(top);
        top = newNode;
        this.size++;
        return true;
    }


    @Override
    public Integer pop() {

        if(isEmpty()){
            return null;
        }

        Node<Integer> temp = top; // Make a copy of first in line

        top = top.getNext(); // Set top to next in line
        size--; // Decrease size of stack
        return temp.getValue();
    }


    @Override
    public boolean isEmpty() {
        return (size == 0);
    }

    @Override
    public boolean isFull() {
        return false;
    }


    @Override
    public Integer peek() {

        if(isEmpty()){
            return null;
        }
        return top.getValue();
    }

    @Override
    public int size() {
        return this.size;
    }

    public void clear(){
        this.size = 0;
        this.top = null;
    }

    public String toString() {
        String rtn = "";

        if (isEmpty()) {
            return "<Empty>";
        }

        Node<Integer> n = top;
        while (n != null) {
            if (n == top) {
                rtn += "top -> ";
            } else {
                rtn += "       ";
            }

            rtn += "Row:" + n.getRow() + ", Col:" + n.getColumn() + ", Val: " + n.getValue() + "\n";
            n = n.getNext();
        }

        return rtn;
    }


}
