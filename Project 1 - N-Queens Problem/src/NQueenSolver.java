public class NQueenSolver {

    private static final int N = 8;
    private static SLinkedStack solution = new SLinkedStack();


    public static void main(String[] args){

        int[][] chessBoard = new int[N][N];

        // Use backtracking, starting at column 0, to find first solution
        if(!solve(chessBoard, 0)) {
            System.out.println("Solution not found!");
        }
        showBoard(chessBoard);
        System.out.println(solution.toString());

    }




    private static void showBoard(int board[][]) {

        // Iterate over rows
        for (int i = 0; i < N; i++) {

            // Iterate over columns
            for (int j = 0; j < N; j++)

                //assert false;
                // If cell contains 1, then Q exists
                if (board[i][j] == 1) {
                    System.out.print("Q\t");
                } else {
                    System.out.print("_\t");
                }
            System.out.println("\n");
        }
    }



    private static boolean inSameRow(int[][] board, int row, int col){

        /*
         * * = reference point
         * @ = check point
         *
         * _ _ _ _
         * _ * @ @
         * _ _ _ _
         * _ _ _ _
         */

        int i;

        for (i = 0; i < col; i++) {

            System.out.println("ROW: Checking for row conflict at (" + row + ", " + i + ")");

            // Check all in $row, e.g. (x,1),(x,2),(x,3),...,(x,8)
            if (board[row][i] == 1) {
                // Queen found!
                System.out.println("ROW: Conflict found at (" + row + ", " + i + ")");
                return true;
            }
        }

        System.out.println("ROW: No conflict found at (" + row + ", " + i + ")");
        return false;
    }


    private static boolean inSameDiag(int[][] board, int row, int col){

        int i, j;
        // Check diagonals for Queens only in columns to the left of the reference row/column.
        // This is because the program is calculating Queen solutions from left column to right.

        /*
         * * = reference point
         * @ = check point
         *
         * @ _ _ _
         * _ @ _ _
         * _ _ @ _
         * _ _ _ *
         */

        // e.g. if reference is (5,5): check (4,4),(3,3),(2,2),(1,1),(0,0) for Queen
        for (i = row, j = col; i >= 0 && j >= 0; i--, j--) {
            if (board[i][j] == 1) {
                System.out.println("DIAG: Southbound Conflict found at (" + i + ", " + j + ")");
                return true;
            }
        }

        /*
         * * = reference point
         * @ = check point
         *
         * _ _ _ *
         * _ _ @ _
         * _ @ _ _
         * @ _ _ _
         */

        // e.g. if reference is (5,5): check (6,4),(7,3)
        for (i = row, j = col; j >= 0 && i < N; i++, j--) {
            if (board[i][j] == 1) {
                System.out.println("DIAG: Northbound Conflict found at (" + i + ", " + j + ")");
                return true;
            }
        }

        System.out.println("DIAG: No conflict found at (" + i + ", " + j + ")");
        return false;
    }


    private static boolean conflict(int[][] board, int row, int col){

        // Check for row out of bounds
        if(row < 0 || row > N){
            System.out.println("DIAG: Cell out of bounds!");
            return false;
        }

        // Check for column out of bounds
        if(col < 0 || col > N){
            System.out.println("DIAG: Cell out of bounds!");
            return false;
        }

        // Return true if Queen in same row or diagonal
        if(inSameRow(board, row, col)){
            return true;
        }

        if(inSameDiag(board, row, col)){
            return true;
        }

        System.out.println("Hit!");
        return false;
    }


    private static boolean solve(int[][] board, int col) {

        if(col >= N){
            // If we have reached the last column, then stack contains all 8 coordinate solutions!
            return true;
        }

        // Check each row
        for (int i = 0; i < N; i++) {

            System.out.println("SOLVE: Checking (" + i + ", " + col + ")");

            // If no conflict at current row
            if (!conflict(board, i, col)) {

                // No conflict! Set Queen at coordinates
                board[i][col] = 1;
                System.out.println("Hit!");

                solution.push(i, col, 1);

                // If we reached the last column, then stop
//                if (col == N)
//                    return true;
                /*
                 * Recursive call!
                 * We are building a state space tree to locate the first complete branch.
                 * After locating a hit, store the solution temporarily in the stack, then check the next leaf (column) on the branch
                 */
                if (solve(board, col + 1)) {
                    // State
                    return true;
                }
                // If backtrack does not return true, that means this branch does not work, in which case we revert and pop bad solution from the stack
                board[i][col] = 0;
                solution.pop();
            }
        }
        // Recursion ended before all solutions could be found...
        return false;

    }



}
