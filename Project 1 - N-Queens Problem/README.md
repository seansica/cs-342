### N-Queens Problem Solver

#### Summary
This program solves the 8 queens problem. The 8 queens problem postulates that you can place 8
queens on a chess board, so that no queen could capture any other. This solution uses the back-track method to determine a solution by recursively searching an NxN sized 2D array (chessboard).

#### UML Diagram
![UML](N-Queens Solver UML.png)
