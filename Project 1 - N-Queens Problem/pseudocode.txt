
Chessboard = int[8][8]

What to Check:

1. Can't be in same row, x-plane --> private boolean inSameRow()
2. Can't be in same column, y-plane --> private boolean inSameColumn()
3. Can't be in same diagonal --> private boolean inSameDiagonal()

Only push correct solutions onto stack, i.e. stack only contains solutions

(0,0)PUSH, (0,1)inSameRow(0):true>skip, (0,2)inSameRow(0,2) ..., (0,8)row same, skip, (0,9)max row exceeded, skip
(1,0)same_col:skip, inSameDiagonal(1,1)

# start with (0,0) pushed
for row on chessboard
    # (0,1),(1,1),(2,1),...,(7,1)
    # inSameRow(0):true>skip, inSameRow(1):true>skip, inSameRow(2):true>skip, ..., inSameRow(8):true>skip
    if not inSameRow()
        # (1,y) hit!
        for column on chessboard
            # (1,0),(1,1),(1,2),(1,3),...,(1,8)
            if not inSameColumn()
                # (1,2) hit !

Handle edges:
if row > static final MAX_NUM_ROWS, then stop
if row < static final MAX_NUM_ROWS, then skip
if col > static final MAX_NUM_COLS, then stop
if col < static final MAX_NUM_COLS, then skip

Need to be able to show status:
- private void main.showBoard() > print 8x8 board with an X in hit cells
- private void stack.toString()


for each cell(x,y); do check $x and $y in stack($cell($x,$y)); done


FINAL SOLUTION ?
[  (7,3)  ] top
[  (6,1)  ]
[  (5,6)  ]
[  (4,2)  ]
[  (3,5)  ]
[  (2,7)  ]
[  (1,4)  ]
[  (0,0)  ] bottom

-/-/-/-/-/
-/*/*/*/-/
-/*/X/*/-/
-/*/*/*/-/
-/-/-/-/-/

X/*/-/-/-/ --> (if * = x, skip)
*/*/-/-/-/ --> (if * =
-/-/-/-/-/
-/-/-/-/-/
-/-/-/-/-/

-/-/-/-/-/
-/*/*/*/-/
-/*/X/*/-/
-/*/*/*/-/
-/-/-/-/-/

Check one column at a time, from left to right.
for i in board[row][i]
    check for conflict
        Check for Q in Same Diagonal:
            Loop 1: Checks all rows, first column only, obviously no conflicts
            Loop 2: Checks all rows, second + first column; fail if Q in either diagonal on left side (remember we are counting from left to right so only check for conflicts on the left
            Loop 3: Checks all rows, third + second + first column, fail condition same as above
        Check for Q in same Row:
            Loop 0-7: Check all rows to see if Q in same row as reference cell; fail if so


    If no conflict
        create a node containing the coordinates of the ref cell
        then push node onto stack. This will be the STATE SPACE TREE

        Recurse over method again, this time checking the next column using the exact same methodology.
        If that method is successful, it will continue pushing correct Q coordinate solutions onto stack until 8 reached

    If conflict
        Remove last pushed node onto stack and try next x/row coordinate
        Eventually we will hit a series of Q coordinates that do not conflict with each other!

