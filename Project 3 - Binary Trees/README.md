### Binary Tree
#### Dracula Metrics

#### Summary
This program populates a binary tree with all words in the novel, Dracula. It does not create nodes for duplicate words. Instead, each node keeps track of the number of occurrences that the word appears in the novel.

The following metrics are calculated by the binary tree:
         
1. Calculates the number of times the following words appear in the novel:
    - transylvania,
    - harker
    - vampire
    - expostulate
    - fang
    - renfield
2. Calculates the max depth of the tree.
3. Calculates the number of unique words in the novel.
4. Calculates what root is stored at the root of the tree.
5. Prints all of the words located at the bottommost level of the tree.
6. Calculates the total number of words in the novel (including duplicates).
7. Determines which word occurs most frequently.
8. Prints the first 20 words in a pre-order, post-order, and in-order traversal of the tree.

#### UML Diagram
##### Overview
![UML](diagrams/overview.png)
##### Main
![UML](diagrams/main.png)
##### Binary Tree
![UML](diagrams/tree.binary.png)
##### Linked List
![UML](diagrams/list.linked.png)