package main;

import java.io.*;


public class Log extends PrintStream {

    private static final String ANSI_RED = "\u001B[31m";
    private static final String ANSI_BLUE = "\u001B[34m";
    private static final String ANSI_RESET = "\u001B[0m";

    private static final String LOGGING_FILEPATH = "/Users/seansica/IdeaProjects/cs-342/Project 3 - Binary Trees/src/parsed_dracula.txt";
    public static final boolean CAPTURE_LOGGING = true;
    public static final boolean PRINT_LOGS = false;


    public Log(OutputStream out) {
        super(out);
    }

    @Override
    public void println(String msg) {
        super.println();

        if(CAPTURE_LOGGING)
            LogHandler(msg);

        if(PRINT_LOGS)
            System.out.println(ANSI_BLUE + msg + ANSI_RESET);
    }



    /**
     * This is a mutator method that appends all DEBUG and ERROR messages to a log file. The path and filename of the log is defined by GroceryStoreConstants.LOGGING_FILEPATH.
     * All log messages are accompanied by a corresponding timestamp.
     * @param msg This is the String which should be appended to the log
     */
    private static void LogHandler(String msg){

        // Initialize timestamp handler
        //String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());

        // Initialize writer placeholder
        Writer writer = null;

        try{
            // Open the file in write mode
            writer = new BufferedWriter(new FileWriter(LOGGING_FILEPATH, true));

            // Append timestamp and debug/error message to log file
            //writer.write(timeStamp + " : " + msg + "\n");
            writer.write(msg + ",\n");

            // Close the file when complete
            writer.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
}
