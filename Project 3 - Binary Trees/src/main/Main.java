package main;

import edu.bu.met.cs342a1ex.TextParser;
import tree.binary.BinaryTree;
import tree.binary.Node;
import tree.binary.NodeNotFoundException;

public class Main {

    private static BinaryTree tree;             // Binary tree to store all words in alphabetical order
    private static long executionTime;          // Total time in ms it takes to build the binary tree
    private static int numberOfUniqueWords;     // Total number of nodes in the tree
    private static int totalNumberOfWords;      // Sum of all count integers of all nodes in the tree
    private static int maxDepth;                // Distance from root to farthest leaf in the tree
    private static String rootWord;             // Word contained in the topmost root node in the tree
    private static Node highestWordCount;       // Node containing the highest count integer in the tree

    private static String[] findWords =         // List of words to determine word counts for if they are present in the tree
            {       "transylvania",
                    "harker",
                    "renfield",
                    "vampire",
                    "expostulate",
                    "fang"      };


    /**
     * Main method to execute program, initialize tree, calculate metrics, and print results
     */
    public static void main(String[] args){

        Main main = new Main();
        main.initialize();

        System.out.printf("*********** [POST PROCESSING] **************\n");

        /*
         * [x] How many times do the following words appear in the text?
         *        transylvania, harker, vampire, expostulate, fang, renfield
         * [x] What is the depth of the tree?
         * [x] How many different words are there in the book?
         * [x] What word is at the root of the tree?
         * [x] Which word(s) are at the deepest leaves in the tree?
         * [x] How many total words are in the book?
         * [x] Which word occurs most frequently?
         * [x] Display the first 20 words in a Pre-Order Traversal of the tree.
         * [x] Display the first 20 words in a Post-Order Traversal of the tree.
         * [x] Display the first 20 words in an In-Order Traversal of the tree.
         *
         */
        System.out.printf("Time to build tree?                                    %dms\n\n", executionTime);

        System.out.println("How many times do the following words appear in the text?");
        findWords(findWords);

        System.out.printf("\nWhat is the depth of the tree?                         %d\n", maxDepth);

        System.out.printf("How many different words are there in the book?        %d\n", numberOfUniqueWords);

        System.out.printf("What word is at the root of the tree?                  %s\n", rootWord);

        System.out.printf("Which word(s) are at the deepest leaves in the tree?   ");
        tree.printLevel(maxDepth);

        System.out.printf("\nHow many total words are in the book?                  %d\n", totalNumberOfWords);

        System.out.printf("Which word occurs most frequently?                     %s : %d\n" , highestWordCount.getWord(), highestWordCount.getCount());

        System.out.printf("\nDisplay the first 20 words in a Pre-Order Traversal of the tree.     ");
        tree.traversePreOrder(20);

        System.out.printf("\nDisplay the first 20 words in a Post-Order Traversal of the tree.    ");
        tree.traversePostOrder(20);

        System.out.printf("\nDisplay the first 20 words in a In-Order Traversal of the tree.      ");
        tree.traverseInOrder(20);


    }


    /**
     * Builds binary tree based on TextParser stream, then calculates required metrics
     */
    private void initialize(){

        tree = new BinaryTree();

        TextParser tp = new TextParser();

        if (!tp.openFile("dracula.txt")) {
            System.out.println("Error opening file");
            System.exit(0);
        }
        String word = tp.getNextWord();

        long startTime = System.currentTimeMillis();
        while(word != null) {
            //System.out.println(word);
            //LogHandler(word);
            tree.insert(word);
            word = tp.getNextWord();
        }
        // *********** [POST PROCESSING] **************
        executionTime = System.currentTimeMillis() - startTime;
        numberOfUniqueWords = tree.getUniqueWordCount();
        totalNumberOfWords = tree.getTotalWordCount();
        maxDepth = tree.getMaxDepth();
        rootWord = tree.getRootWord();
        highestWordCount = tree.highestCount();
    }


    /**
     * Determines the word count for all numbers in the provided list of words.
     * If the word does not exist in the tree, a NodeNotFoundException message is printed.
     * @param findWords Refers to the list of words to search for.
     */
    private static void findWords(String[] findWords){


        for (int i = 0; i < findWords.length; i++) {

            try{
                Node found = tree.find(findWords[i]);
                System.out.printf("    (%s) = %d time(s)\n", found.getWord(), found.getCount());
            }
            catch (NodeNotFoundException e){
                System.out.println(e.getMessage());
            }
        }
    }

}
