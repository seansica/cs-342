package tree.binary;

import list.linked.LinkedList;

public class BinaryTree{

    private Node root; // maintain constant reference to the root node in the tree


    /**
     * Default constructor
     */
    public BinaryTree(){
        root = null;
    }


    /**
     * Adds a new leaf node to the tree based on alphabetical order
     * @param word Refers to the data that should be stored in the new leaf node
     */
    public void insert(String word){

        //System.out.printf("Trying to insert word: '%s'\n", word);

        if(root == null){
            //System.out.printf("(%s) set as root!\n", word);
            root = new Node(word);
        }
        else
            root = insertRecursive(root, new Node(word));

    }

    /**
     * Recursively determines where to store the new node based on alphabetical order.
     * Only inserts new nodes on the bottom of the tree.
     * Alphabetical order is determined by String compareTo result
     * @param currentParent Determines the node at which to start attempting to recursively add the new node
     * @param newNode Refers to the new node to insert
     * @return Recursive stack of all in order Nodes in the tree in order to retain proper pointer to root
     */
    private Node insertRecursive(Node currentParent, Node newNode) {

        if (currentParent == null) {
            //System.out.printf("SUCCESS: set new leaf (%s)\n", newNode.word);
            return newNode;
        }

        else if (areTheSame(currentParent.word, newNode.word)){
            //System.out.printf("Incremented (%s) count to %d\n", currentParent.word, currentParent.count);
            currentParent.count++;
        }

        else if (comesAfter(currentParent.word, newNode.word)) {
            //System.out.printf("RECURSE: trying insert(%s) to the right of (%s)\n", newNode.word, currentParent.word);
            currentParent.right = insertRecursive(currentParent.right, newNode);
        }

        else if (comesBefore(currentParent.word, newNode.word)) {
            //System.out.printf("RECURSE: trying insert(%s) to the left of (%s)\n", newNode.word, currentParent.word);
            currentParent.left = insertRecursive(currentParent.left, newNode);
        }

        return currentParent;
    }


    /**
     * Comparator method to determine whether to insert nodes on the right
     * @param compare This should be the existing node in the tree
     * @param to This should be the new node
     * @return True if new node alphabetically proceeds after the existing node
     */
    private boolean comesAfter(String compare, String to) {
        int wordCompare = compare.compareToIgnoreCase(to);
        return (wordCompare < 0);
    }


    /**
     * Comparator method to determine whether to insert nodes on the left
     * @param compare This should be the existing node in the tree
     * @param to This should be the new node
     * @return True if new node alphabetically precedes the existing node
     */
    private boolean comesBefore(String compare, String to) {
        int wordCompare = compare.compareToIgnoreCase(to);
        return (wordCompare > 0);
    }


    /**
     * Comparator method to determine whether to increment node's word count
     * @param compare This should be the existing node in the tree
     * @param to This should be the new node
     * @return True if both nodes contain the same word (case is ignored)
     */
    private boolean areTheSame(String compare, String to) {
        int wordCompare = compare.compareToIgnoreCase(to);
        return (wordCompare == 0);
    }


    /**
     * Public method to locate a Node based on a given word
     * @param word This is the data that the method is searching for
     * @return The Node containing the reference word
     * @throws NodeNotFoundException Throw an exception if the word is not contained in the tree
     */
    public Node find(String word) throws NodeNotFoundException {

        Node found = new Node(word);

        if(root.word.equals(word)){
            //System.out.printf("Found node (%s) at root\n", root.word);
            found = root;
        }
        else{
            found = findRecursive(root, found);
        }
        return found;
    }


    /**
     * Recursively locates a Node based on a given word
     * @param currentParent Refers to the Node at which to start searching
     * @param found Container Node to return if the word is found
     * @return The Node containing the reference word
     * @throws NodeNotFoundException Throw an exception if the word is not contained in the tree
     */
    private Node findRecursive(Node currentParent, Node found) throws NodeNotFoundException {

        if (currentParent == null) {
            throw new NodeNotFoundException("    Failed to find any occurrences of word ("+ found.word + ")");
        }

        else if (areTheSame(currentParent.word, found.word)){
            //System.out.printf("SUCCESS: found node (%s)\n", found.word);
            found = currentParent;
            return found;
        }

        else if (comesAfter(currentParent.word, found.word)) {
            //System.out.printf("RECURSE: trying find(%s) to the right of (%s)\n", found.word, currentParent.word);
            currentParent = findRecursive(currentParent.right, found);
        }

        else if (comesBefore(currentParent.word, found.word)) {
            //System.out.printf("RECURSE: trying find(%s) to the left of (%s)\n", found.word, currentParent.word);
            currentParent = findRecursive(currentParent.left, found);
        }

        return currentParent;
    }


    /**
     * Prints the nodes in pre-order up to a given number of nodes
     * @param numberOfWords Determines how many words to print
     */
    public void traversePreOrder(int numberOfWords) {

        //Node postOrder = root;
        LinkedList list = traversePreOrderRecursive(root, new LinkedList());
        list.showUpTo(numberOfWords);
    }


    /**
     * Recursively traverses all nodes in pre-order and stores them in a list
     * @param current Node at which to start printing
     * @param list Container list to store nodes in pre-order
     * @return A Linked List containing all nodes tree in order of the traversal path
     */
    private LinkedList traversePreOrderRecursive(Node current, LinkedList list) {

        /*
         * Algorithm Preorder(tree)
         * 1. Visit the root.
         * 2. Traverse the left subtree,
         * 3. Traverse the right subtree
         */

        // break condition
        if (current == null)
            return list;

        // first print the current node
        list.insert(current.word);

        // then recur on left subtree
        traversePreOrderRecursive(current.left, list);

        // lastly recur on right subtree
        traversePreOrderRecursive(current.right, list);

        return list;
    }


    /**
     * Prints the nodes in post-order up to a given number of nodes
     * @param numberOfWords Determines how many words to print
     */
    public void traversePostOrder(int numberOfWords) {

        LinkedList list = traversePostOrderRecursive(root, new LinkedList());
        list.showUpTo(numberOfWords);
    }


    /**
     * Recursively traverses all nodes in post-order and stores them in a list
     * @param current Node at which to start printing
     * @param list Container list to store nodes in post-order
     * @return A Linked List containing all nodes tree in order of the traversal path
     */
    private LinkedList traversePostOrderRecursive(Node current, LinkedList list) {

        if (current == null)
            return list;

        // first recur on left subtree
        traversePostOrderRecursive(current.left, list);

        // then recur on right subtree
        traversePostOrderRecursive(current.right, list);

        // now deal with the node
        list.insert(current.word);
        return list;
    }

    /**
     * Prints the nodes in order up to a given number of nodes
     * @param numberOfWords Determines how many words to print
     */
    public void traverseInOrder(int numberOfWords){


        LinkedList list = traverseInorderRecursive(root, new LinkedList());
        list.showUpTo(numberOfWords);
    }


    /**
     * Recursively traverses all nodes in order and stores them in a list
     * @param current Node at which to start printing
     * @param list Container list to store nodes in order
     * @return A Linked List containing all nodes tree in order of the traversal path
     */
    private LinkedList traverseInorderRecursive(Node current, LinkedList list) {

        if (current != null) {

            traverseInorderRecursive(current.left, list);

            list.insert(current.word);

            traverseInorderRecursive(current.right, list);
        }
        return list;
    }

    /**
     * Public method that calculates the max depth from the root node to the farthest leaf
     * @return max depth integer
     */
    public int getMaxDepth(){
        return getDepthRecursive(root);
    }

    /**
     * Recurse the tree to determine the distance from a reference node to the farthest leaf
     * @param root is the starting node from which to start counting
     * @return max depth integer
     */
    private int getDepthRecursive(Node root) {

        // Root being null means tree doesn't exist.
        if (root == null)
            return 0;

        // Get the depth of the left and right subtree using recursion
        int leftDepth = getDepthRecursive(root.left);
        int rightDepth = getDepthRecursive(root.right);

        // Choose the larger one and add the root to it
        if (leftDepth > rightDepth)
            return (leftDepth + 1);
        else
            return (rightDepth + 1);
    }


    /**
     * Determines if the tree is empty or not
     * @return true if empty or false if not empty
     */
    public boolean isEmpty() {
        return root == null;
    }


    /**
     * Public method that calculates the total number of unique nodes in the tree
     * @return
     */
    public int getUniqueWordCount() {
        return getUniqueWordCountRecursive(root);
    }


    /**
     * Recursively calculates the total number of unique nodes starting from a reference parent node
     * @param current Determines the node at which to start recurs'ing down
     * @return total number of unique nodes integer
     */
    private int getUniqueWordCountRecursive(Node current) {
        if (current == null)
            return 0;

        return getUniqueWordCountRecursive(current.left) + 1 + getUniqueWordCountRecursive(current.right);
    }


    /**
     * Public method that calculates the total number of words in the tree as determined by the sum of all Node.count integers
     * @return sum of all count integers of all nodes in the tree
     */
    public int getTotalWordCount() {
        return getTotalWordCountRecursive(root);
    }


    /**
     * Recursively calculates the sum of all count integers starting at a reference parent node
     * @param current Determines the node at which to start recurs'ing down
     * @return Sum of all count integers of all touched nodes in the tree
     */
    private int getTotalWordCountRecursive(Node current) {

        return current == null ? 0 : getTotalWordCountRecursive(current.left) + current.count + getTotalWordCountRecursive(current.right);
    }


    /**
     * Public method to calculate the node with the highest word count in the tree
     * @return Node with the highest word count
     */
    public Node highestCount(){

        Node highestCount = root;
        highestCount = findHighestCount(root, highestCount);
        return highestCount;
    }


    /**
     * Recursively locates the Node with the highest word count in the tree based on a starting reference parent node
     * @param current Refers to the Node at which to start searching
     * @param highest Is the container Node for the return value
     * @return Node containing the highest word count
     */
    private Node findHighestCount(Node current, Node highest){

        if (current == null) {
            return highest;
        }

        if (current.count > highest.count) {
            highest = current;
        }

        else if (current.count < highest.count){
            findHighestCount(current.left, highest);
            findHighestCount(current.left, highest);
        }

        return highest;
    }


    /**
     * Public method to print all leaf nodes at a given level
     * @param level Determines the level of leafs to print
     */
    public void printLevel(int level){
        printLevel(root, level);
    }


    /**
     * Recursive function to print all nodes of a given level from left to right
     * @param node Determines the Node at which to start traversing
     * @param level Determines the leaf level to print
     * @return True once complete
     */
    private boolean printLevel(Node node, int level){
        // base case
        if (node == null) {
            return false;
        }

        if (level == 1)
        {
            System.out.print(node.word + " ");

            // return true if at-least one node is present at given level
            return true;
        }

        boolean left = printLevel(node.left, level - 1);
        boolean right = printLevel(node.right, level - 1);

        return left || right;
    }


    /**
     * Getter method to return the word stored in the root node
     * @return Word stored in the root node
     */
    public String getRootWord() {
        return root.word;
    }
}
