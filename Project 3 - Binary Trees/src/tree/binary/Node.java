package tree.binary;

public class Node {

    protected String word;      // refers to the word contained within the node

    protected Node left;    // refers to left node leaf

    protected Node right;   // refers to right node leaf

    protected int count;    // refers to the number of occurrences that value
                            // is added to tree

    public Node(){
        word = null;
        count = 1;
        left = right = null;
    }

    public Node(String word){
        this.word = word;
        count = 1;
        left = right = null;
    }

    public Node(Node node){
        this.word = node.word;
        this.left = node.left;
        this.right = node.right;
        this.count = node.count;
    }

    /**
     * The result is a negative integer if this.word precedes the given word.
     * The result is a positive integer if this.word follows the given word.
     * The result is zero if the words are equal.
     * @param word
     * @return
     */
    public int compare(Node node) {
        return this.word.compareToIgnoreCase(node.word);
    }

    public Node getLeft() {
        return this.left;
    }

    public Node getRight(){
        return this.right;
    }

    public String getWord(){
        return this.word;
    }

    public int getCount() {
        return this.count;
    }
}
