package tree.binary;

public class NodeNotFoundException extends Exception {

    public NodeNotFoundException(String errMessage){
        super(errMessage);
    }
}
