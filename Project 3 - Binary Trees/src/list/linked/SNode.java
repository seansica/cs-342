package list.linked;

public class SNode{

    private SNode next;
    private String data;

    public SNode(String data){
        super();
        this.data = data;
        next = null;
    }

    public SNode getNext(){
        return next;
    }

    public void setNext(SNode next){
        this.next = next;
    }

    public String getData(){
        return this.data;
    }

    public void setData(String data){
        this.data = data;
    }

}
